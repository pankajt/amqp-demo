package com.amqp.signup.utils;

public class NameExtractor {
	private static final String REGEX_ALL_SPACES = "\\s+";

	public static String getFirstName(String fullName) {
		String[] splits = splitName(fullName);
		return splits[0];
	}

	public static String getLastName(String fullName) {
		String[] splits = splitName(fullName);
		return splits[splits.length - 1];
	}

	private static String[] splitName(String fullName) {
		fullName.replaceAll(REGEX_ALL_SPACES, " "); // convert multiple spaces to single space
		String[] split = fullName.split(" ");
		return split;
	}
}
