package com.amqp.signup.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CountryCodePrepender {
	private static Logger logger = LoggerFactory.getLogger(CountryCodePrepender.class);
	
	private static final String FRENCH_CODE = "+33";
	private static final String REGEX_ALL_SPACES = "\\s+";
	
	public static String addCountryCode(String phoneNumber) {
		logger.debug("Processing user phone: {}", phoneNumber);
		phoneNumber.replaceAll(REGEX_ALL_SPACES, "");
		if (phoneNumber.indexOf(FRENCH_CODE) > -1) {
			return phoneNumber;
		} else {
			return FRENCH_CODE + phoneNumber;
		}
	}
}
