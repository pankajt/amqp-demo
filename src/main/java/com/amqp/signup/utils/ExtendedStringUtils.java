package com.amqp.signup.utils;

public class ExtendedStringUtils {
    public static String stripDoubleQuotes(String string) {
        return string.replaceAll("^\"|\"$", "");
    }
}
