package com.amqp.signup.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ThreadApplicationRunner implements ApplicationRunner {
	
	private Logger logger = LoggerFactory.getLogger(ThreadApplicationRunner.class);
	
    @Autowired
    AsynchronousService service;

    @Override
    public void run(ApplicationArguments args) throws Exception {
    	logger.info("Starting consumer thread application");
        service.executeAsynchronously();
    }

}