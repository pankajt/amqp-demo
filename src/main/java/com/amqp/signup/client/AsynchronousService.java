package com.amqp.signup.client;

import com.amqp.signup.utils.ExtendedStringUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import com.amqp.signup.model.SignupInfo;
import com.amqp.signup.model.SignupInfoResponse;
import com.amqp.signup.processor.IMessageProcessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AsynchronousService {
	private Logger logger = LoggerFactory.getLogger(AsynchronousService.class);

	private Object lock = new Object();

    @Autowired
    private TaskExecutor taskExecutor;
    
    @Autowired
    RabbitTemplate template;
    
	@Autowired
	ObjectMapper mapper;

	@Autowired
	IMessageProcessor processor;
    
    public void executeAsynchronously() {
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
            	while(true) {
                	logger.debug("Asynchronous service started");
                    Message m = template.receive("signups", 10000);
                    if (m == null) continue;
                    logger.info("Yay! Consumer thread processing the message: {}", m);
                    handleMessage(m.getBody());
            	}
            }

            private void handleMessage(byte[] bytes) {
				processor.reset();
				String json = new String(bytes);
				if (json == null) {
					return;
				}

				JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
				String wrappedObject = ExtendedStringUtils.stripDoubleQuotes(jsonObject.get("wrappedObject").toString());
				String className = ExtendedStringUtils.stripDoubleQuotes(jsonObject.get("className").toString());
				logger.debug("Loading object with content {} and class name {}", wrappedObject, className);
				try {
					Object object = mapper.readValue(wrappedObject, getClassType(className));
					processor.process(object);
					synchronized (lock) {
						logger.info("Notifying receiver that object is ready");
						lock.notify();
					}
				} catch (JsonProcessingException e) {
					logger.error("Could not load object from string", e);
				}
			}

			private Class<?> getClassType(String className) {
				Class<?> classType = null;
				try {
					classType = Class.forName(className);
				} catch (ClassNotFoundException e) {
					logger.error("Could not get class type information", e);
				}
				return classType;
			}
        });
    }

	public Object getLockObject() {
		return lock;
	}
}