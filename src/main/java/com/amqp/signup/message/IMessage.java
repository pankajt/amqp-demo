package com.amqp.signup.message;

public interface IMessage {
	public Object getWrappedObject();
	
	public void setWrappedObject(Object obj);
	
	public String getClassName();

	public void setClassName(String className);
}
