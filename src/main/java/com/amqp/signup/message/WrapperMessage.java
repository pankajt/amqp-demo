package com.amqp.signup.message;

public class WrapperMessage implements IMessage {

	private Object wrappedObject;
	private String className;

	public Object getWrappedObject() {
		return wrappedObject;
	}

	public void setWrappedObject(Object wrappedObject) {
		this.wrappedObject = wrappedObject;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
}
