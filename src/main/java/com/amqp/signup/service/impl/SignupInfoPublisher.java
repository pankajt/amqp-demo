package com.amqp.signup.service.impl;

import com.amqp.signup.client.AsynchronousService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amqp.signup.config.SignupQueueProperties;
import com.amqp.signup.message.IMessage;
import com.amqp.signup.message.WrapperMessage;
import com.amqp.signup.service.IPublisher;

@Component
public class SignupInfoPublisher implements IPublisher{
	private Logger logger = LoggerFactory.getLogger(SignupInfoPublisher.class);
	
	@Autowired
	private SignupQueueProperties props;

	@Autowired
	private RabbitTemplate template;
	
	@Autowired
	private SignupInfoReceiver receiver;

	@Autowired
	AsynchronousService service;

	public Object publishMessage(Object object) throws InterruptedException {
		logger.debug("Publishing message {}", object.toString());
		IMessage message = getWrapperMessage(object);
		template.convertAndSend(props.getExchangeName(), props.getRoutingKey(), message);

//		Object lock = receiver.getLockObject(); // for message listener
		Object lock = service.getLockObject(); // for consumer thread
		Object obj = null;
		synchronized (lock) {
			logger.info("Waiting for message to be read by the receiver and processed");
			lock.wait();
			obj = receiver.getProcessedObject();
		}
		return obj;
	}

	private IMessage getWrapperMessage(Object object) {
		IMessage message = new WrapperMessage();
		message.setWrappedObject(object);
		message.setClassName(object.getClass().getName());
		return message;
	}		
}
