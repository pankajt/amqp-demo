package com.amqp.signup.service.impl;

import com.amqp.signup.model.SignupInfo;
import com.amqp.signup.processor.IMessageProcessor;
import com.amqp.signup.service.IReceiver;
import com.amqp.signup.utils.ExtendedStringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SignupInfoReceiver implements IReceiver<SignupInfo> {

    private Logger logger = LoggerFactory.getLogger(SignupInfoReceiver.class);

    private Object lock = new Object();

    @Autowired
    ObjectMapper mapper;


    @Autowired
    IMessageProcessor processor;

    public void handleMessage(byte[] bytes) {
        processor.reset();
        String json = new String(bytes);
        if (json == null) {
            return;
        }

        JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
        String wrappedObject = ExtendedStringUtils.stripDoubleQuotes(jsonObject.get("wrappedObject").toString());
        String className = ExtendedStringUtils.stripDoubleQuotes(jsonObject.get("className").toString());
        logger.debug("Loading object with content {} and class name {}", wrappedObject, className);
        try {
            Object object = mapper.readValue(wrappedObject, getClassType(className));
//            processor.process(object);
            synchronized (lock) {
                logger.info("Notifying receiver that object is ready");
                lock.notify();
            }
        } catch (JsonProcessingException e) {
            logger.error("Could not load object from string", e);
        }
    }

    private Class<?> getClassType(String className) {
        Class<?> classType = null;
        try {
            classType = Class.forName(className);
        } catch (ClassNotFoundException e) {
            logger.error("Could not get class type information", e);
        }
        return classType;
    }

    public Object getLockObject() {
        return lock;
    }

    public Object getProcessedObject() {
        return processor.getResult();
    }
}
