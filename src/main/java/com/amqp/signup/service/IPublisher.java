package com.amqp.signup.service;

public interface IPublisher {
	public Object publishMessage(Object object) throws InterruptedException;
}
