package com.amqp.signup.service;

public interface IReceiver<T> {
	public void handleMessage(byte[] bytes);
}
