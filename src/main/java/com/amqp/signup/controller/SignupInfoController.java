package com.amqp.signup.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amqp.signup.model.SignupInfo;
import com.amqp.signup.model.SignupInfoResponse;
import com.amqp.signup.service.impl.SignupInfoPublisher;

@RestController
public class SignupInfoController {
	
	private Logger logger = LoggerFactory.getLogger(SignupInfoController.class);
	
	@Autowired
	SignupInfoPublisher publisher;
	
	@RequestMapping(value="/", method = RequestMethod.POST)
	public ResponseEntity<SignupInfoResponse> publish(@Valid @RequestBody SignupInfo userinfo) {
		SignupInfoResponse signupInfoResponse = null;
		try {
			signupInfoResponse = (SignupInfoResponse) publisher.publishMessage(userinfo);
		} catch(Exception e) {
			logger.error("Exception calling publish", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<SignupInfoResponse>(signupInfoResponse, HttpStatus.OK);
	}
}
