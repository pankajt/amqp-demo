package com.amqp.signup.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class SignupInfo{

	@NotEmpty(message = "{name.not.empty}")
	@Pattern(regexp = "^([^0-9]*)$", message = "{name.contain.numbers}")
	private String fullName;

	@NotEmpty(message = "{phone.not.empty}")
	private String phone;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String toString() {
		return "fullName: " + fullName + " phone: " + phone;
	}
}
