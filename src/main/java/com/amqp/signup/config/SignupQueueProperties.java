package com.amqp.signup.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SignupQueueProperties {

	@Value("${com.amqp.signup.queue}")
	private String queueName;

	@Value("${com.amqp.signup.exchange}")
	private String exchangeName;

	@Value("${com.amqp.signup.routing-key}")
	private String routingKey;

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}

	public String getRoutingKey() {
		return routingKey;
	}

	public void setRoutingKey(String routingKey) {
		this.routingKey = routingKey;
	}

}
