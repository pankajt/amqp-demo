package com.amqp.signup.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amqp.signup.service.IReceiver;

@Configuration
public class SignupQueueConfiguration {

	@Autowired
	SignupQueueProperties props;

	@Autowired
	ConnectionFactory connectionFactory;

//	@Autowired
//	MessageListenerAdapter messageListener;

	@Bean
	public Queue signupQueue() {
		return new Queue(props.getQueueName(), false);
	}

	@Bean
	TopicExchange signupExchange() {
		return new TopicExchange(props.getExchangeName());
	}

	@Bean
	Binding doBinding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(props.getRoutingKey());
	}

//	@Bean
//	MessageListenerAdapter messageListenerAdapter(IReceiver<?> receiver) {
//		return new MessageListenerAdapter(receiver);
//	}

	@Bean
	public Jackson2JsonMessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

//	@Bean
//	SimpleMessageListenerContainer getContainer() {
//		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//		container.setConcurrentConsumers(2);
//		container.setConnectionFactory(connectionFactory);
//		container.setQueueNames(props.getQueueName());
//		container.setMessageListener(messageListenerAdapter);
//		return container;
//	}
}
