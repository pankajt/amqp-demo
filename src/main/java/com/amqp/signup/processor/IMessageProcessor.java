package com.amqp.signup.processor;

public interface IMessageProcessor<T extends Object> {
	public void reset();
	public T getResult();
	public void process(Object object);
}
