package com.amqp.signup.processor.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amqp.signup.model.SignupInfo;
import com.amqp.signup.model.SignupInfoResponse;
import com.amqp.signup.processor.IMessageProcessor;
import com.amqp.signup.utils.CountryCodePrepender;
import com.amqp.signup.utils.NameExtractor;

@Component
public class SignupInfoProcessor implements IMessageProcessor<SignupInfoResponse> {

    private Logger logger = LoggerFactory.getLogger(SignupInfoProcessor.class);

    private SignupInfoResponse signupInfoResponse;

    @Override
    public void reset() {
        logger.info("Resetting processor object");
        signupInfoResponse = null;
    }

    public SignupInfoResponse getResult() {
    	if (signupInfoResponse == null) {
    		logger.warn("Response message is empty");
			return null;
		}
    	logger.info("Returning response message: {}", signupInfoResponse.toString());
		return signupInfoResponse;
	}

    @Override
    public void process(Object object) {
        SignupInfo signupInfo = (SignupInfo) object;
        logger.debug("Processing user with name: {}", signupInfo.getFullName());
        signupInfoResponse = new SignupInfoResponse();
        signupInfoResponse.setFirstName(NameExtractor.getFirstName(signupInfo.getFullName()));
        signupInfoResponse.setLastName(NameExtractor.getLastName(signupInfo.getFullName()));
        signupInfoResponse.setPhoneNumber(CountryCodePrepender.addCountryCode(signupInfo.getPhone()));
        logger.info("User {} {} with phone {} has just signed up!", signupInfoResponse.getFirstName(),
                signupInfoResponse.getLastName(), signupInfoResponse.getPhoneNumber());
        logger.info("Object processed: {}", signupInfoResponse.toString());
    }

}
